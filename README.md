URL de la web actualmente: http://colppy-te-loadbala-1o3zytahx511m-975238966.us-east-1.elb.amazonaws.com/

La infraestructura esta armada en AWS, se implemento mediante un codigo en CloudFormation con formato yaml. El mismo se compone de 6 partes:

Los 2 Security Group, uno para el loadbalancer y uno para EC2
El LoadBalancer
El autoscaling
Y por ultimo el Template de EC2 para el autoscaling

Se exponen los puertos 80 y 22, el 80 por la salida a la web por http y el 22 para la conexion ssh para el envio de archivos.

La infraestructura se desplego a través de la pipeline, la misma se ejecuta con 2 tags.

Uno de los tags despliega la infrastructura, ejectua el template en CloudFormation y espera a que se cree el loadbalancer, luego devuelve el URL

El otro despliega cambios del index.html, los sube directamente a la instancia activa amazon, se genero un script que busque el nombre de las instancias activas en el momento.

La descripcion del CloudFormation mediante una plantilla es la siguiente
 
[!alt plantilla](plantilla.jpg "Plantilla")
